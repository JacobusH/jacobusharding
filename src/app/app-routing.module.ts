import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./@frontend/frontend.module')
      .then(m => m.FrontendModule),
  },
  // {
  //   path: 'admin',
  //   loadChildren: () => import('./@admin/admin.module') // TODO: figure out how to put an absolute path here
  //     .then(m => m.AdminModule),
  // },
  // {
  //   path: 'user',
  //   loadChildren: () => import('./@user/user.module')
  //     .then(m => m.UserModule),
  // },
  { path: '', redirectTo: 'about', pathMatch: 'full' },
  { path: '**', redirectTo: 'about' },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
