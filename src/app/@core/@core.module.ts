import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenBgDirective } from './directives/gen-bg.directive';

const COMPONENTS = [
  // GenBgComponent
];

const DIRECTIVES = [
  GenBgDirective
];

@NgModule({
  declarations: [
    ... COMPONENTS
    , ... DIRECTIVES
    ],
  imports: [
    CommonModule
  ],
  exports: [
    ... COMPONENTS
    , ... DIRECTIVES
  ]
})
export class CoreModule { }
