import { Directive, ElementRef, Renderer2, Input, AfterViewInit, OnChanges, SimpleChanges, HostListener} from '@angular/core';
import { SVG } from '@svgdotjs/svg.js'
import * as chroma from "chroma-js/"

declare var chroma: any;

@Directive({
  selector: '[appGenBg]'
})
export class GenBgDirective implements AfterViewInit {
  @Input() appGenBg;
  num_recs_1D = 25;
  num_colors_scale = 30;
  draw;
  grid_2D;
  colors_x;
  colors_y;
  svg_cont_x;
  svg_cont_y;
  id;
  is_jitter = true;

  constructor(
    private el: ElementRef
    , private renderer2: Renderer2) {
      // this.draw = SVG().addTo(this.el.nativeElement).size(window.innerWidth, window.innerHeight)


  }

  ngAfterViewInit() {
    this.svg_cont_x = this.appGenBg.clientWidth+500;
    this.svg_cont_y = this.appGenBg.clientHeight+500;
    this.colors_x = chroma.scale(this.getRandomColorScale()).colors(this.num_colors_scale)
    this.colors_y = chroma.scale(this.getRandomColorScale()).colors(this.num_colors_scale)
    this.draw = SVG().addTo(this.el.nativeElement).addClass('svg').size(this.svg_cont_x, this.svg_cont_y)

    this.makeGrid();
    // this.drawRecs();
    this.drawTris();
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    clearTimeout(this.id);
    this.id = setTimeout(() =>  {
      this.el.nativeElement.removeChild(document.getElementsByClassName('svg')[0])
      this.svg_cont_x = this.appGenBg.clientWidth+500;
      this.svg_cont_y = this.appGenBg.clientHeight+500;
      this.colors_x = chroma.scale(this.getRandomColorScale()).colors(this.num_colors_scale)
      this.colors_y = chroma.scale(this.getRandomColorScale()).colors(this.num_colors_scale)
      this.draw = SVG().addTo(this.el.nativeElement).addClass('svg').size(this.svg_cont_x, this.svg_cont_y)
      this.makeGrid();
      this.drawTris();
    }, 100)
  }

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  makeGrid() {
    this.grid_2D = [...Array(this.num_recs_1D+1)].map(e => Array(this.num_recs_1D+1)) //.fill({'x': -1, 'y': -1}));
    for(let i = 0; i <= this.num_recs_1D; i++) {
      for(let j = 0; j <= this.num_recs_1D; j++) {
        let len_x_side = (this.svg_cont_x / this.num_recs_1D);
        let len_y_side = (this.svg_cont_y / this.num_recs_1D);
        let x = i * len_x_side;
        let y = j* len_y_side;
        // this.grid_2D.push(this.draw.rect({width: len_x_side, height: len_y_side, x: x, y: y}).attr({ fill: '#f06' }))

        let top_left_x = (i * len_x_side);
        let top_left_y = (j * len_y_side);

        let top_right_x = top_left_x + len_x_side;
        let top_right_y = top_left_y;

        let bot_left_x = top_left_x;
        let bot_left_y = top_left_y + len_y_side;

        let bot_right_x = bot_left_x + len_x_side;
        let bot_right_y = top_right_y + len_y_side;
        // let coords = ''+ top_left_x+','+top_left_y +' '+ ''+ top_right_x+','+top_right_y +' '+ ''+ bot_right_x+','+bot_right_y+ ' '+ bot_left_x+','+bot_left_y;

        // jitter
        if(this.is_jitter) {
          let val_x = top_left_x;
          let val_y = top_left_y;
          let jitter_val_x = (this.appGenBg.clientWidth / this.num_recs_1D)*0.4
          let jitter_val_y = (this.appGenBg.clientHeight / this.num_recs_1D)*0.5

          top_left_x = this.randomIntFromInterval(val_x-jitter_val_x, val_x+jitter_val_x);
          top_left_y = this.randomIntFromInterval(val_y-jitter_val_y, val_y+jitter_val_y);
        }

        this.grid_2D[i][j] = [top_left_x, top_left_y]
      }
    }
  }

  drawRecs() {
    for(let i = 0; i < this.grid_2D.length-1; i++) {
      for(let j = 0; j < this.grid_2D.length-1; j++) {
        let coords =
          ''+   this.grid_2D[i][j][0] +','+ this.grid_2D[i][j][1]
          +' '+ this.grid_2D[i+1][j][0] +','+ this.grid_2D[i+1][j][1]
          +' '+ this.grid_2D[i+1][j+1][0] +','+ this.grid_2D[i+1][j+1][1]
          +' '+ this.grid_2D[i][j+1][0] +','+ this.grid_2D[i][j+1][1];
        this.draw.polygon(coords).fill('#fff').stroke({ width: 2 }).attr({ fill: this.getColorIdx(i,j) })
      }
    }
  }

  drawTris() {
    for(let i = 0; i < this.grid_2D.length-1; i++) {
      for(let j = 0; j < this.grid_2D.length-1; j++) {
        let coords =
          ''+   this.grid_2D[i][j][0] +','+ this.grid_2D[i][j][1]
          +' '+ this.grid_2D[i+1][j][0] +','+ this.grid_2D[i+1][j][1]
          +' '+ this.grid_2D[i][j+1][0] +','+ this.grid_2D[i][j+1][1];
        this.draw.polygon(coords).stroke({ width: 2 }).attr({ fill: this.getColorIdx(i,j) })
        coords =
          ''+   this.grid_2D[i+1][j][0] +','+ this.grid_2D[i+1][j][1]
          +' '+ this.grid_2D[i+1][j+1][0] +','+ this.grid_2D[i+1][j+1][1]
          +' '+ this.grid_2D[i][j+1][0] +','+ this.grid_2D[i][j+1][1];
        this.draw.polygon(coords).stroke({ width: 2 }).attr({ fill: this.getColorIdx(i,j) })
      }
    }
  }

  getColorIdx(i,  j, num_points = 3) {
    let color_x = -1;
    let color_y = -1;
    if(num_points == 3) { // triangles
      color_x = this.colors_x[this.scaleColorIdx(Math.floor((this.grid_2D[i][j][0] + this.grid_2D[i+1][j][0]) / 2), 0, this.svg_cont_x)]
      color_y = this.colors_y[this.scaleColorIdx(Math.floor((this.grid_2D[i][j][1] + this.grid_2D[i][j+1][1]) / 2), 0, this.svg_cont_y)]
    }
    else if(num_points == 4) { // squares (doesn't actually matter...)
      color_x = this.colors_x[this.scaleColorIdx(Math.floor((this.grid_2D[i][j][0] + this.grid_2D[i+1][j][0]) / 2), 0, this.svg_cont_x)]
      color_y = this.colors_y[this.scaleColorIdx(Math.floor((this.grid_2D[i][j][1] + this.grid_2D[i][j+1][1]) / 2), 0, this.svg_cont_y)]
    }
    return chroma.mix(color_x, color_y, 0.5).hex();
  }


  scaleColorIdx(x, minny, maxxy) {
    // scale x into range [a,b]
    let a = 0;
    let b = this.num_colors_scale-1;
    return Math.round((b-a)*((x-minny) / (maxxy-minny))+a)
  }

  getRandomColorScale(num_points = 2) {
    let start_point = this.randomIntFromInterval(0, 360);
    if(num_points == 2) {
      let opp_point = (start_point > 360/2) ? (start_point/2) : (start_point*2)
      return [chroma.hsv(start_point, 1, 0.7).hex(), chroma.hsv(opp_point, 1, 0.7).hex()]
    }
  }

  randomIntFromInterval(min, max) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
}



