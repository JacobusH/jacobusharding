import { Component, OnInit, OnDestroy } from '@angular/core';
// import { VersionService } from 'app/@core/services/_index';
import { ActivatedRoute, Router}  from '@angular/router';
import { Subscription } from 'rxjs';


@Component({
  selector: 'frontend',
  templateUrl: './frontend.component.html',
  styleUrls: ['./frontend.component.scss']
})
export class FrontendComponent implements OnInit, OnDestroy {
  r$: Subscription;
  vs$: Subscription;
  appVersion;
  isHome = false;

  constructor(
    private actRoute: ActivatedRoute
    , private router: Router
    ) {

  }

  ngOnInit() {

  }

  ngOnDestroy() {

  }

}
