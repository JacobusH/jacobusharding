import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { FrontendComponent } from './frontend.component';
import { AboutComponent } from './about/about.component';
import { HireComponent } from './hire/hire.component';
import { ProjectsComponent } from './projects/projects.component';


const routes: Routes = [
    { path: '', component: FrontendComponent,
      children: [
        { path: '', component: AboutComponent },
        { path: 'hire', component: HireComponent },
        { path: 'projects', component: ProjectsComponent },
      ]
    },
    { path: '',redirectTo: '', pathMatch: 'full' },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FrontendRoutingModule {
}
