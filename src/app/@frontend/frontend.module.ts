import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CoreModule } from 'app/@core/@core.module';
import { FormsModule } from '@angular/forms';
import { FrontendRoutingModule } from './frontend-routing.module';
// import { NgScrollbarModule } from 'ngx-scrollbar';

import { AboutComponent } from './about/about.component';
import { HireComponent } from './hire/hire.component';
import { ProjectsComponent } from './projects/projects.component';
import { FrontendComponent } from './frontend.component';

const COMPONENTS = [
  AboutComponent
  , HireComponent
  , ProjectsComponent
  , FrontendComponent
];

const MODULES = [
  FrontendRoutingModule
    , CommonModule
    , CoreModule
    , FormsModule
    , RouterModule
    // , NgScrollbarModule
];

@NgModule({
  declarations: [
    ... COMPONENTS
  ],
  imports: [
    ... MODULES
  ]
})
export class FrontendModule { }
