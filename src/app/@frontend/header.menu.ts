export interface MenuItem {
  'title': string,
  'icon'?: string,
  'link': string,
  'children'?: MenuItem[]
}

export const MENU_ITEMS: MenuItem[] = [
  {
    title: 'About',
    icon: 'earth',
    link: 'about'
  },
  {
    title: 'Contact',
    icon: 'home',
    link: 'contact'
  },
  {
    title: 'Lessons',
    icon: 'video-outline',
    link: 'lessons',
    children: [
      {
        title: 'Program',
        icon: 'file-document-outline',
        link: 'lessons/program'
      },
      {
        title: 'Material',
        icon: 'file-document-outline',
        link: 'lessons/materials'
      },
      {
        title: 'Teachers',
        icon: 'file-document-outline',
        link: 'lessons/teachers'
      },
      {
        title: 'References',
        icon: 'file-document-outline',
        link: 'lessons/references'
      },
    ]
  },
  {
    title: 'Music',
    icon: 'image-multiple',
    link: 'music',
    children: [
      {
        title: 'Tutorials',
        icon: 'file-document-outline',
        link: 'music/tutorials'
      },
      {
        title: 'Videos',
        icon: 'file-document-outline',
        link: 'music/videos'
      },
    ]
  },
];
