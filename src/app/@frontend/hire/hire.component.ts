import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-hire',
  templateUrl: './hire.component.html',
  styleUrls: ['./hire.component.scss']
})
export class HireComponent implements OnInit {
  windowWidth;


  constructor() {
    this.windowWidth = window.innerWidth;
  }

  ngOnInit(): void {
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.windowWidth = event.target.innerWidth;
    // console.log(event.target.innerWidth);
  }

}
